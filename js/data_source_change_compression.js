{
    "use strict"

    let form = document.getElementById('data-source-change-compression')

    let change_compression = function () {

        let formData = new FormData(form)
        let data = {}
        formData.forEach(function(value, key){
            data[key] = value
        })
        fetch(form.action, {
            method: 'PATCH',
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "X-CSRFToken": csrftoken,
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            body: JSON.stringify(data),
        })
            .then(function (response) {
                if (response.status === 200) {
                    window.loadAndShowMessages()
                } else if (response.status === 400) {
                    response.json().then(function (object) {
                        console.error(response.status, object)
                        window.loadAndShowMessages()
                    })
                } else {
                    console.error(response.status)
                    window.location.replace(window.location.href)  // todo: refactor
                }
            })
            .catch(error => console.error(`Fetch Error =\n`, error))
    }

    form.addEventListener('submit', function(e) {
        e.preventDefault()
        change_compression()
    })

    document.getElementById('id_compression').addEventListener("change", function () {
        change_compression()
    })

}

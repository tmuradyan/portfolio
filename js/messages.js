(function (window) {
    'use strict';

    let showMessage = function (message) {
        let title, text, msg;

        if (message.message.includes('///')) {
            msg = message.message.split('///');
            title = msg[0];
            text = msg[1];
        } else {
            title = message.message;
            text = '&zwnj;';  // margin-bottom fix
        }

        new PNotify({
            title: title,
            text: text,
            type: message.level_tag,
            addclass: 'stack-bar-bottom',
            stack: {
                "dir1": "up",
                "dir2": "right",
                "spacing1": 0,
                "spacing2": 0
            },
            width: "70%"
        });

    };

    let loadAndShowMessages = function () {

        fetch('/messages/', {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
        })
            .then(function (response) {
                if (response.status === 200) {
                    response.json().then(function (object) {
                        object.messages.forEach(function (message) {
                            showMessage(message);
                        });
                    });
                } else {
                    console.error(response.status);
                }
            })
            .catch(error => console.error(`Fetch Error =\n`, error));
    };

    if (document.readyState === "loading") {
        document.addEventListener("DOMContentLoaded", loadAndShowMessages);
    } else {  // `DOMContentLoaded` already fired
        loadAndShowMessages();
    }

    window.showMessage = showMessage;
    window.loadAndShowMessages = loadAndShowMessages;

})(window);

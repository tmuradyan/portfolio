{
    'use strict';

    var store = {
        debug: true,
        state: {
            dedicatedSpace: 0,
            slaPlan: '',
            serviceProviders: []
        },
        setDedicatedSpaceAction: function (newValue, updateWidget = true) {
            if (this.debug) console.log('setDedicatedSpaceAction triggered with', newValue)

            let value = (newValue <= dedicatedSpaceMaxValue) ? newValue : dedicatedSpaceMaxValue
            this.state.dedicatedSpace = value

            if (updateWidget || newValue !== value) {
                document.getElementById('id_dedicated_space').value = value
            }
        },
        setSlaPlanAction (newValue, updateWidget = true) {
            if (this.debug) console.log('setSlaPlanAction triggered with', newValue)

            this.state.slaPlan = newValue

            if (updateWidget) {
                document.getElementById(`id_sla_plan_${newValue}`).checked = true
            }

            // Update required count and make decision about service providers
            if (this.getSelectedServiceProvidersCountAction(false) > this.getRequiredServiceProvidersCountAction()) {
                // Need deselect
                let deselectCount = this.getSelectedServiceProvidersCountAction(false) - this.getRequiredServiceProvidersCountAction(false)
                this.state.serviceProviders.slice(0, deselectCount) // todo: change algorithm of deselecting
                    .forEach((code) => this.removeServiceProviderAction(code))
            } else {
                // Only sync status and update required count
                this.syncAvailableStatusOfSelectableServiceProvidersAction()
            }

        },
        addServiceProviderAction (newValue) {
            if (this.debug) console.log('setServiceProviderAction triggered with', newValue)

            this.state.serviceProviders.push(newValue)

            // Add hidden input
            let serviceProviderInputHiddenElement = document.createElement("input")
            serviceProviderInputHiddenElement.type = 'hidden'
            serviceProviderInputHiddenElement.name = 'data_centers'
            serviceProviderInputHiddenElement.id = `id_service_provider_${newValue}`
            serviceProviderInputHiddenElement.value = newValue
            document.getElementById('project-create').appendChild(serviceProviderInputHiddenElement)

            // Select
            document.getElementById(newValue).classList.add('data-center-selected')

            // Update selected count and disable all not selected choices if selected is equal to required...
            this.syncAvailableStatusOfSelectableServiceProvidersAction()

        },
        removeServiceProviderAction (oldValue) {
            if (this.debug) console.log('removeServiceProviderAction triggered with', oldValue)

            let index = this.state.serviceProviders.indexOf(oldValue)

            if (index > -1) {
                this.state.serviceProviders.splice(index, 1)

                // Remove hidden input
                let serviceProviderInputHiddenElement = document.getElementById(`id_service_provider_${oldValue}`)
                serviceProviderInputHiddenElement.parentNode.removeChild(serviceProviderInputHiddenElement)

                // Deselect
                document.getElementById(oldValue).classList.remove('data-center-selected')

                // Update selected count and make selectable all disabled choices if selected is not equal to required...
                this.syncAvailableStatusOfSelectableServiceProvidersAction()

            }
        },
        syncAvailableStatusOfSelectableServiceProvidersAction() {
            if (this.debug) console.log('syncAvailableStatusOfSelectableServiceProvidersAction triggered')

            let selectableServiceProviders = jmespath.search(serviceProviders, `[*].code`).filter(code => !this.state.serviceProviders.includes(code))
            if (this.getSelectedServiceProvidersCountAction() !== this.getRequiredServiceProvidersCountAction()) {
                selectableServiceProviders.forEach(code => document.getElementById(code).classList.remove('data-center-not-available'))
            } else {
                selectableServiceProviders.forEach(code => document.getElementById(code).classList.add('data-center-not-available'))
            }
        },
        getSelectedServiceProvidersCountAction () {
            if (this.debug) console.log('getSelectedServiceProvidersCountAction triggered')

            let selectedServiceProvidersCount = this.state.serviceProviders.length

            document.getElementById('selected-service-providers-count').textContent = selectedServiceProvidersCount

            return selectedServiceProvidersCount
        },
        getRequiredServiceProvidersCountAction () {
            if (this.debug) console.log('getRequiredServiceProvidersCountAction triggered')

            let requiredServiceProvidersCount = jmespath.search(slaPlans, `[?code=='${this.state.slaPlan}'] | [0].process_count`)

            document.getElementById('required-service-providers-count').textContent = requiredServiceProvidersCount

            return requiredServiceProvidersCount
        },
        calculateTotalPricePerBackupJob () {
            return _.sumBy(window.serviceProviders, (o) => { return (store.state.serviceProviders.includes(o.code)) ? o.backup_price_per_job : 0})
        },
        calculateTotalStoragePricePerGigabyteHour () {
            return _.sumBy(window.serviceProviders, (o) => { return (store.state.serviceProviders.includes(o.code)) ? o.storage_price_per_gigabyte_hour : 0})
        },
        getServiceProviderStoragePricePerGigabyteHour (serviceProvider) {
            return jmespath.search(window.serviceProviders, `[?code=='${serviceProvider}']`)[0].storage_price_per_gigabyte_hour
        },
        getServiceProviderName (serviceProvider) {
            return jmespath.search(window.serviceProviders, `[?code=='${serviceProvider}']`)[0].name
        },
        getTotalEstimatedCostPerDay () {
            return this.calculateTotalPricePerBackupJob() + (this.calculateTotalStoragePricePerGigabyteHour() * 24 * this.state.dedicatedSpace)
        },
        getTotalEstimatedCostPerMonth () {
            return this.getTotalEstimatedCostPerDay() * 31
        },
        getTotalEstimatedCostPerYear () {
            return this.getTotalEstimatedCostPerDay() * 366
        },
        calculate () {
            document.getElementById('invoice').innerHTML = `
                <table id="invoice" class="table table-responsive-md invoice-items">
                    <thead>
                        <tr class="text-dark">
                            <th id="cell-item" class="font-weight-semibold">Backup Service</th>
                            <th id="cell-price" class="text-center font-weight-semibold">$${(store.calculateTotalPricePerBackupJob()).toFixed(2)} / day</th>
                        </tr>
                    </thead>
                    <tbody id="invoice-body">
                        <tr>
                            <td>Services, connections and data sources</td> 
                            <td class="text-center">unlimited</td> 
                        </tr>
                        <tr>
                            <td>Automatic backups</td> 
                            <td class="text-center">no / daily / weekly</td> 
                        </tr>
                        <tr>
                            <td>Manual backups</td> 
                            <td class="text-center">unlimited</td> 
                        </tr>
                        <tr>
                            <td colspan="2">Backup monitoring</td> 
                        </tr>
                        <tr>
                            <td>Encrypted backup storage</td>
                            <td class="text-center">BLAKE2b-256 and AES-256-CTR</td>
                        </tr>
                        <tr>
                            <td>Compression algorithm settings</td> 
                            <td class="text-center">lz4 / zstd / zlib / lzma</td> 
                        </tr>
                        <tr>
                            <td colspan="2">Data deduplication</td> 
                        </tr>
                    </tbody>
                </table>
                <table id="invoice" class="table table-responsive-md invoice-items">
                    <thead>
                        <tr class="text-dark">
                            <th id="cell-item" class="font-weight-semibold">Reserved Space Storage</th>
                            <th id="cell-price" class="text-center font-weight-semibold">${store.state.dedicatedSpace} GB</th>
                        </tr>
                    </thead>
                    <tbody id="invoice-body">${store.state.serviceProviders
                        .map((serviceProvider, index) => `
                        <tr>
                            <td>${this.getServiceProviderName(serviceProvider)}</td>
                            <td class="text-center">$${this.getServiceProviderStoragePricePerGigabyteHour(serviceProvider)} / hour per 1 GB</td>
                        </tr>`)
                        .join(" ")}
                    </tbody>
            </table>
            <table id="invoice" class="table table-responsive-md invoice-items">
                <thead>
                    <tr class="text-dark">
                        <th id="cell-item" class="font-weight-semibold">Network</th>
                        <th id="cell-price" class="text-center font-weight-semibold">free</th>
                    </tr>
                </thead>
                <tbody id="invoice-body">
                    <tr>
                        <td>Incoming/outgoing traffic</td> 
                        <td class="text-center">free</td> 
                    </tr>
                </tbody>
            </table>
            <table id="invoice" class="table table-responsive-md invoice-items">
                <thead>
                    <tr class="text-dark">
                        <th id="cell-item" class="font-weight-semibold">Iron SLA Plan</th>
                        <th id="cell-price" class="text-center font-weight-semibold">free</th>
                    </tr>
                </thead>
                <tbody id="invoice-body">
                    <tr>
                        <td>Automatic backup</td> 
                        <td class="text-center">up to 1 time per day for each data source</td> 
                    </tr>
                    <tr>
                        <td>Service providers</td> 
                        <td class="text-center">1</td> 
                    </tr>
                    <tr>
                        <td>Support response time</td> 
                        <td class="text-center">up to 3 business days</td> 
                    </tr>
                </tbody>
            </table>`

            document.getElementById('invoice-summary').innerHTML = `
            <div class="invoice-summary">
                <div class="row justify-content-end">
                    <div class="col-sm-4">
                        <table class="table h6 text-dark">
                            <tbody>
                                <tr class="b-top-0">
                                    <td class="text-right">Total Estimated Cost</td>
                                    <td class="text-right">$${(store.getTotalEstimatedCostPerDay()).toFixed(2)} per 1 day</td>
                                </tr>
                                <tr class="b-top-0">
                                    <td class="text-right text-1" colspan="2">$${(store.getTotalEstimatedCostPerMonth()).toFixed(2)} per 1 month</td>
                                </tr>
                                <tr class="b-top-0">
                                    <td class="text-right text-1" colspan="2">$${(store.getTotalEstimatedCostPerYear()).toFixed(2)} per 1 year</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>`
        }
    }
    window.store = store

    // Decide, show form or not, and if yes, load initial state values to form inputs
    let loadInitialValues = function () {

        fetch('/projects/create/', {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
        })
            .then(function (response) {
                if (response.status === 200) {
                    response.json().then(function (object) {
                        const data = object.data
                        const validators = data.project_create.validators
                        const fieldsValidators = validators.fields
                        const dedicatedSpaceMaxValue = fieldsValidators[0].options.max_value
                        window.dedicatedSpaceMaxValue = dedicatedSpaceMaxValue
                        // const formValidators = validators.form
                        const initial = data.project_create.initial
                        const defaultDedicatedSpace = initial.dedicated_space
                        const slaPlans = data.sla_plans
                        window.slaPlans = slaPlans
                        const defaultSlaPlanCode = initial.sla_plan
                        const defaultSlaPlanData = jmespath.search(slaPlans, `[?code=='${defaultSlaPlanCode}'] | [0]`)
                        const defaultSlaPlanProcessCount = defaultSlaPlanData.process_count
                        const serviceProviders = data.service_providers
                        window.serviceProviders = serviceProviders

                        // it should have enough data centers to be selected
                        // data_centers.filter(free > DEFAULT_DEDICATED_SPACE).count() > <DEFAULT_SLA_PLAN>.process_count
                        const availableServiceProviderCount = jmespath.search(serviceProviders, `length([?free>'${defaultDedicatedSpace}'])`)
                        let containerElement
                        if (availableServiceProviderCount >= defaultSlaPlanProcessCount) {
                            containerElement = document.getElementsByClassName('project-create-form-container')[0]
                        } else {
                            containerElement = document.getElementsByClassName('project-create-no-form-container')[0]
                        }
                        containerElement.classList.remove("d-none")

                        // Update dedicated space max value
                        document.getElementById('id_dedicated_space').max = dedicatedSpaceMaxValue

                        // Render SLA plans list
                        document.getElementById('sla-plans-container').innerHTML = slaPlans
                            .map((slaPlan, index) => `<div id="${slaPlan.code}" class="custom-control custom-radio">
                                    <input type="radio" name="sla_plan" value="${slaPlan.code}" id="id_sla_plan_${slaPlan.code}" class="custom-control-input" tabindex="${index + 10}"${index === 0? ' required': ''}>
                                    <label class="custom-control-label" for="id_sla_plan_${slaPlan.code}" id="id_sla_plan_${slaPlan.code}_label">${slaPlan.name}</label>
                                </div>`)
                            .join(" ") // todo: input: {% if sla_plan.disabled %} disabled{% endif %} label: {% if sla_plan.disabled %} sla-plan-disabled{% endif %}
                        // console.log(dedicatedSpaceMaxValue, defaultDedicatedSpace, defaultSlaPlan, serviceProviders, availableServiceProviderCount)

                        // Render service providers list
                        document.getElementById('service-providers-container').innerHTML = serviceProviders
                            .map((serviceProvider, index) => `<div id="${serviceProvider.code}" class="d-flex flex-row data-center-row border rounded p-2 mb-2">
                                    <div>
                                        <img class="mr-3 data-center-logo data-center-${serviceProvider.code}-logo" src="${serviceProvider.logo}" alt="${serviceProvider.name}">
                                    </div>
                                    <div>
                                        <h3>${serviceProvider.name} <a href="${serviceProvider.url}" target="_blank"><i class="fa fa-xs fa-external-link"></i></a></h3>
                                        <p>${serviceProvider.code === 'online' ? serviceProvider.description + ' / $' + serviceProvider.storage_price_per_gigabyte_hour + ' ' + gettext('per GB') : '&nbsp;'}</p>
                                    </div>
                                </div>`)  // show prices only for available service providers, todo: refactor, replace `serviceProvider.code == 'online'` to some function/settings
                            .join(" ")

                        // 2-way binding for store
                        document.getElementById('id_dedicated_space').addEventListener("change", event => {
                            store.setDedicatedSpaceAction(event.target.value, false)
                        })

                        slaPlans.forEach(slaPlan => {
                            document.getElementById(`id_sla_plan_${slaPlan.code}`).addEventListener("change", event => {
                                store.setSlaPlanAction(event.target.value, false)
                            })
                        })

                        // Disabled for MVP, only one choice for now
                        // serviceProviders.forEach(serviceProvider => {
                        //     document.getElementById(serviceProvider.code).addEventListener("click", event => {
                        //         if (!event.target.classList.contains('data-center-not-available')) {
                        //             if (!event.target.classList.contains('data-center-selected')) {
                        //                 store.addServiceProviderAction(event.target.id)
                        //             } else {
                        //                 store.removeServiceProviderAction(event.target.id)
                        //             }
                        //         }
                        //     })
                        // })

                        // Load initial to Store
                        store.setDedicatedSpaceAction(defaultDedicatedSpace)
                        store.setSlaPlanAction(defaultSlaPlanCode)

                        serviceProviders
                            .filter(sp => sp.free > defaultDedicatedSpace)
                            .sort((a, b) => a.free - b.free)
                            .slice(-defaultSlaPlanProcessCount)
                            .forEach((sp) => store.addServiceProviderAction(sp.code))

                        // Disable some SLA plans for MVP
                        document.getElementById(`id_sla_plan_bronze`).disabled = true
                        document.getElementById(`id_sla_plan_bronze_label`).classList.add('sla-plan-disabled')
                        document.getElementById(`id_sla_plan_silver`).disabled = true
                        document.getElementById(`id_sla_plan_silver_label`).classList.add('sla-plan-disabled')
                        document.getElementById(`id_sla_plan_gold`).disabled = true
                        document.getElementById(`id_sla_plan_gold_label`).classList.add('sla-plan-disabled')

                    })
                } else {
                    console.error(response.status)
                }
            })
            .catch(error => console.error(`Fetch Error =\n`, error))
    }

    if (document.readyState === "loading") {
        document.addEventListener("DOMContentLoaded", loadInitialValues)
    } else {  // `DOMContentLoaded` already fired
        loadInitialValues()
    }
}

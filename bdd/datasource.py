import datetime

from behave import given, when, then
from django.apps import apps
from django.urls import reverse


@given('a several data sources')  # deprecated, todo: replace me to exact single resource request
def create_data_sources(context):
    PostgreSQLDataSource = apps.get_model('backupnerdspostgresql', 'DataSource')
    MariaDBDataSource = apps.get_model('backupnerdsmariadb', 'DataSource')
    SSHDataSource = apps.get_model('backupnerdsssh', 'DataSource')

    context.data_sources = {
        'postgresql': PostgreSQLDataSource.active.create(name='PostgreSQL Data Source', connection=context.connections['postgresql'][0]),
        'mariadb': MariaDBDataSource.active.create(name='MariaDB Data Source', connection=context.connections['mariadb'][0]),
        'ssh': SSHDataSource.active.create(directory='SSH Data Source', connection=context.connections['ssh'][0]),
    }

    for data_source in context.data_sources.values():

        status, backups = apps.get_model('backupnercore', 'Backup').create_backup_processes_for_data_source(data_source=data_source)

        if not status:
            raise OSError


@when('I navigate to create "{service_type}" data source create page')
def navigate_to_data_source_create_page(context, service_type):
    context.service_type = service_type
    context.connection = context.connections[service_type][0]
    url = context.connection.get_create_data_source_url()
    context.response = context.test.client.get(url, follow=True)
    context.test.assertEqual(context.response.status_code, 200)


@when('I navigate to project data source list page')
def navigate_to_project_data_source_list_page(context):
    context.project = context.projects[0]
    context.response = context.test.client.get(reverse('projects:project_data_sources', kwargs={'project_uuid': context.project.uuid}), follow=True)
    context.test.assertEqual(context.response.status_code, 200)


@when('I navigate to project "{service_type}" data source list page')
def navigate_to_project_service_type_data_source_list_page(context, service_type):
    context.project = context.projects[0]
    context.service_type = service_type
    context.response = context.test.client.get(
        reverse(f'projects:project_service_type_data_sources', kwargs={'project_uuid': context.project.uuid, 'service_type': context.service_type}), follow=True)


@when('I navigate to data source detail page')
def navigate_to_data_source_detail_page(context):
    context.project = context.projects[0]
    context.data_source = context.data_sources['mariadb']
    context.response = context.test.client.get(reverse('proto:data_source_detail', kwargs={'data_source_uuid': context.data_source.uuid}), follow=True)
    context.test.assertEqual(context.response.status_code, 200)


@when('I navigate to project nonexistent service type data source jobs by day page')
def navigate_to_project_nonexistent_service_type_data_source_jobs_by_day_page(context):
    context.project = context.projects[0]
    context.data_source = context.data_sources['mariadb']
    context.response = context.test.client.get(reverse('projects:data_source_jobs_by_days', kwargs={'project_uuid': context.project.uuid,
                                                                                                    'data_source_uuid': context.data_source.uuid,
                                                                                                    'service_type': 'noservicetype'}), follow=True)


@when('I navigate to data source jobs by day list page')
def navigate_to_data_source_jobs_by_day_list_page(context):
    context.project = context.projects[0]
    context.data_source = context.data_sources['mariadb']
    context.response = context.test.client.get(reverse('core:data_source_jobs_by_day', kwargs={'data_source_uuid': context.data_source.uuid,
                                                                                               'job_date': datetime.datetime.today().strftime('%Y-%m-%d')}), follow=True)


@when('I navigate to "{service_type}" data source detail page')
def navigate_to_project_data_source_detail_page(context, service_type):
    context.service_type = service_type
    context.data_source = context.data_sources[context.service_type]
    context.response = context.test.client.get(context.data_source.get_absolute_url(), follow=True)
    context.test.assertEqual(context.response.status_code, 200)


@when('send form with new backup compression settings "{compression}"')
def send_form_with_new_compression_backup_settings(context, compression):
    context.compression = compression if compression != 'None' else None

    api_url = context.data_source.get_change_compression_url()
    context.response = context.test.client.patch(api_url, {'compression': context.compression}, content_type='application/json')
    context.test.assertEqual(context.response.status_code, 200)


@when('send form with new backup frequency settings "{frequency}"')
def send_form_with_new_frequency_backup_settings(context, frequency):
    context.frequency = frequency if frequency != 'None' else None

    api_url = context.data_source.get_change_frequency_url()
    context.response = context.test.client.patch(api_url, {'frequency': context.frequency}, content_type='application/json')
    context.test.assertEqual(context.response.status_code, 200)


@when('I send form with correct data source data "{name}" "{compression}" "{frequency}"')
def send_form_with_correct_data_source_data(context, name, compression, frequency):
    connection = context.connections[context.service_type][0]

    form_data = {
        'compression': compression if compression != 'None' else '',
        'frequency': frequency if frequency != 'None' else '',
    }

    if context.service_type in ['postgresql', 'mariadb']:
        form_data.update({
            'name': name,
        })
    elif context.service_type == 'ssh':
        form_data.update({
            'directory': name,
        })
    else:
        raise NotImplementedError

    url = connection.get_create_data_source_url()
    context.response = context.test.client.post(url, form_data, follow=True)
    context.test.assertEqual(context.response.status_code, 200)


@when('I confirm "{service_type}" data source delete operation')
def confirm_data_source_delete_operation(context, service_type):
    data_source = context.data_sources[service_type]

    url = data_source.get_absolute_url()
    context.data_source = data_source
    context.response = context.test.client.delete(url, follow=True)
    context.test.assertIn(context.response.status_code, [204, 409])


@then('a "{service_type}" data source should be set inactive')
def check_data_source_inactive(context, service_type):
    context.test.assertTrue(context.service_types[service_type]['data_source_model'].objects.filter(id=context.data_source.id, is_active=False).exists())


@then('a "{service_type}" data source should update backup compression settings')
def check_data_source_change_compression_update(context, service_type):
    filters = {
        'uuid': context.data_source.uuid,
        'compression': context.compression if context.compression != '' else None,
    }

    context.test.assertTrue(context.service_types[service_type]['data_source_model'].active.filter(**filters).exists())


@then('a "{service_type}" data source should update backup frequency settings')
def check_data_source_change_frequency_update(context, service_type):
    filters = {
        'uuid': context.data_source.uuid,
        'frequency': context.frequency if context.frequency != '' else None,
    }

    context.test.assertTrue(context.service_types[service_type]['data_source_model'].active.filter(**filters).exists())


@then('a new "{service_type}" data source "{name}" should be created')
def check_data_source_created(context, service_type, name):
    if service_type in ['postgresql', 'mariadb']:
        filters = {'name': name}
    elif service_type == 'ssh':
        filters = {'directory': name}
    else:
        raise NotImplementedError
    context.test.assertTrue(context.service_types[service_type]['data_source_model'].active.filter(**filters).exists())


@then('I should get data source list page')
def get_data_source_list_page(context):
    context.test.assertEqual(context.response.status_code, 200)


@then('I should get data source jobs by day list page')
def get_data_source_jobs_by_day_list_page(context):
    context.test.assertEqual(context.response.status_code, 200)


@then('I should get "{service_type}" data source list page')
def get_service_type_data_source_list_page(context, service_type):
    context.test.assertEqual(context.response.status_code, 200)


@then('I should get data source detail page')
def get_data_source_detail_page(context):
    context.test.assertEqual(context.response.status_code, 200)


@then('data source backups should be set inactive')
def check_data_source_backups_set_inactive(context):
    context.test.assertFalse(apps.get_model('backupnercore', 'Backup').active.filter(**{f'{context.service_type}_data_source': context.data_source}).exists())


@then('data source busy intervals should be set inactive')
def check_data_source_busy_intervals_set_inactive(context):
    context.test.assertFalse(context.data_source.busy_intervals.filter(is_active=True).exists())

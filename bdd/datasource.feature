Feature: Data Source
  User can add data source and choose connection to fetch it
  
  Scenario Outline: Add data source <service_type>
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    When I navigate to create "<service_type>" data source create page
    And I send form with correct data source data "<name>" "<compression>" "<frequency>"
    Then a new "<service_type>" data source "<name>" should be created
    And I should be redirected to connection page

    Examples: DataSource Connection Types
        | service_type      | name               | compression | frequency |
        | postgresql        | data_source_test1  | None        | None      |
        | postgresql        | data_source_test2  | lz4         | daily     |
        | postgresql        | data_source_test3  | zstd6       | weekly    |
        | postgresql        | data_source_test4  | zlib3       | no        |
        | mariadb           | data_source_test5  | None        | daily     |
        | mariadb           | data_source_test6  | lz4         | weekly    |
        | mariadb           | data_source_test7  | lzma6       | no        |
        | mariadb           | data_source_test8  | zlib3       | None      |
        | ssh               | data_source_test9  | None        | weekly    |
        | ssh               | data_source_test10 | lz4         | no        |
        | ssh               | data_source_test11 | zstd6       | None      |
        | ssh               | data_source_test12 | zlib3       | daily     |

  Scenario: View data source list
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to project data source list page
    Then I should get data source list page

  Scenario Outline: View <service_type> data source list
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to project "<service_type>" data source list page
    Then I should get "<service_type>" data source list page

    Examples: DataSource Service Types
        | service_type          |
        | mariadb               |
        | postgresql            |
        | ssh                   |

  Scenario: View data source detail page
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to data source detail page
    Then I should get data source detail page

  Scenario: View data source jobs by day list
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to data source jobs by day list page
    Then I should get data source jobs by day list page

  Scenario Outline: Update data source backup compression settings
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to "<service_type>" data source detail page
    And send form with new backup compression settings "<compression>"
    Then a "<service_type>" data source should update backup compression settings

    Examples: DataSource Backup Compression Settings
        | service_type      | compression   |
        | postgresql        | None          |
        | postgresql        | lz4           |
        | postgresql        | zstd6         |
        | postgresql        | zlib3         |
        | mariadb           | None          |
        | mariadb           | lz4           |
        | mariadb           | lzma6         |
        | mariadb           | zlib3         |
        | ssh               | None          |
        | ssh               | lz4           |
        | ssh               | zstd6         |
        | ssh               | zlib3         |

  Scenario Outline: Update data source backup frequency settings
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to "<service_type>" data source detail page
    And send form with new backup frequency settings "<frequency>"
    Then a "<service_type>" data source should update backup frequency settings

    Examples: DataSource Backup Frequency Settings
        | service_type      | frequency   |
        | postgresql        | None        |
        | postgresql        | no          |
        | postgresql        | daily       |
        | postgresql        | weekly      |
        | mariadb           | None        |
        | mariadb           | no          |
        | mariadb           | daily       |
        | mariadb           | weekly      |
        | ssh               | None        |
        | ssh               | no          |
        | ssh               | daily       |
        | ssh               | weekly      |

  Scenario Outline: Delete data source
    Given an auth "regular" user
    And a several service types
    And a several servers
    And a several IP addresses
    And a several nodes
    And a several projects
    And a several services
    And a several connections for different service types
    And a several data sources
    When I navigate to "<service_type>" data source detail page
    And I confirm "<service_type>" data source delete operation
    Then a "<service_type>" data source should be set inactive
    And data source backups should be set inactive
    And data source busy intervals should be set inactive

    Examples: Data Source Types
        | service_type  |
        | mariadb       |
        | postgresql    |
        | ssh           |

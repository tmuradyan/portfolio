import functools

from django.apps import apps
from django.core.validators import MinValueValidator
from django.db import models, transaction
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from backupner.apps.proto.models import BaseModel
from backupner.apps.identifiers.utils import uuid1imp
from panel.settings import SLA_PLANS, SERVICE_TYPES

uuid1_for_model = functools.partial(uuid1imp, "backupnerprojects")


class Project(BaseModel):
    """
    Project
    """
    SLA_PLAN_CHOICES = tuple([(sla_plan['code'], sla_plan['name']) for sla_plan in SLA_PLANS])

    uuid = models.UUIDField(verbose_name=_('UUID'), unique=True, default=functools.partial(uuid1_for_model, "Project"), editable=False)
    name = models.CharField(_('name'), max_length=80, db_index=True)
    account = models.ForeignKey('backupnerauth.Account', models.PROTECT, related_name="projects", related_query_name="project", verbose_name=_('account'))
    storage_nodes = models.ManyToManyField('backupnerstorages.Node', through='backupnerstorages.Storage', verbose_name=_('storage nodes'), blank=True,
                                           related_name="storage_projects", related_query_name="storage_project", help_text=_('Servers with storage resources used by project.'))
    dedicated_space = models.PositiveIntegerField(_('dedicated space, GB'), validators=[MinValueValidator(1)])
    sla_plan = models.CharField(_('sLA plan'), max_length=20, choices=SLA_PLAN_CHOICES, db_index=True)

    class Meta:
        verbose_name = _('project')
        verbose_name_plural = _('projects')
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('projects:project_detail', args=[str(self.uuid)])

    def deactivate(self):
        """Deactivate"""
        if self.is_active:
            service_types = [service_type['code'] for service_type in SERVICE_TYPES]
            service_types.append(service_types.pop(service_types.index('ssh')))  # Move 'ssh' to the end of list
            with transaction.atomic():
                # Deactivate services
                for service_type in service_types:
                    services = apps.get_model(f'backupnerds{service_type}', 'Service').active.filter(project=self)
                    [service.deactivate() for service in services]

                # Deactivate storages
                storages = self.storages.filter(is_active=True)
                [storage.deactivate() for storage in storages]
                self.is_active = False
                self.save()

    def get_sla_plan_name(self):
        """ Return SLA Plan name. """
        sla_plan = next((sla_plan for sla_plan in SLA_PLANS if sla_plan['code'] == self.sla_plan), None)
        return sla_plan['name']

    def get_sla_plan_process_count(self):
        """ Return SLA Plan process count. """
        sla_plan = next((sla_plan for sla_plan in SLA_PLANS if sla_plan['code'] == self.sla_plan), None)
        return sla_plan['process_count']

    def get_used_space(self):
        """ Return used space by all related storages. """
        storages = self.storages.filter(is_active=True)

        return sum(storage.get_used_space() for storage in storages)

    def get_service_type_data_sources_count(self, service_type):
        """ Return data sources count in current project and selected service type. """
        return apps.get_model(f'backupnerds{service_type}', 'DataSource').active.filter(connection__service__project=self).count()

    def get_data_sources_count(self):
        """ Return data sources count in current project. """
        return sum(map(self.get_service_type_data_sources_count, [service_type['code'] for service_type in SERVICE_TYPES]))

    def get_available_tunnels(self):
        """ Return available tunnels queryset. """
        tunnels = apps.get_model('backupnerdsssh', 'Connection').active.filter(service__project=self, is_allowed_as_tunnel=True).order_by('service__server__hostname', 'user')

        return tunnels

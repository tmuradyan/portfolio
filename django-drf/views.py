import json
from itertools import chain

from django.apps import apps
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse, Http404
from django.templatetags.static import static
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import requires_csrf_token, csrf_exempt
from django.views.generic import TemplateView
from django.views.generic.base import TemplateResponseMixin, ContextMixin, View
from django.views.generic.edit import FormMixin, ProcessFormView

from rest_framework.generics import DestroyAPIView
from rest_framework.permissions import IsAuthenticated

from backupner.apps.auth.mixins import CanUseServicePermissionRequiredMixin
from backupner.apps.auth.permissions import IsNotSuperuser, IsNotStaff
from backupner.apps.proto.mixins import ProjectMixin, DeactivateMixin
from backupner.apps.proto.views import AdjustedListView

from panel.settings import SERVICE_TYPES, TUNNEL_SERVICES_DESCRIPTION_LIMIT, DEFAULT_SLA_PLAN, DEFAULT_DEDICATED_SPACE, SLA_PLANS, SERVICE_PROVIDERS
from .forms import CreateProjectForm, RenameProjectForm
from .models import Project
from .permissions import IsProjectOwnedByUserAccount


class CreateProjectView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, TemplateResponseMixin, FormMixin, ProcessFormView):
    """
    Create new project
    """

    form_class = CreateProjectForm
    template_name = 'projects/project_create.html'

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""

        if request.content_type != 'application/json':
            # Render form as html
            return self.render_to_response(self.get_context_data())

        # Process fetch request

        validators = {
            'fields': [
                {
                    'field': 'dedicated_space',
                    'code': 'max_value',
                    'options': {
                        'max_value': apps.get_model('backupnerstorages', 'Node').find_max_node_free_space()
                    },
                },
            ],
            'form': [
                {
                    # count of selected data centers should be equal sla plan requirements
                    # <data_centers>.count() == <sla_plan>.process_count
                    'code': 'data_centers_respect_sla_plan',
                },
                {
                    # it should have free space on any of servers inside this data center
                    # <data_centers>.filter(free > <dedicated_space>) == <data_centers>
                    'code': 'free_resources_availability',
                },
            ],
        }

        initial = {
            'dedicated_space': DEFAULT_DEDICATED_SPACE,
            'sla_plan': DEFAULT_SLA_PLAN,
            # <data_centers> should have calculated initial value based on <dedicated_space>, <sla_plans>
        }

        service_providers_free_space = apps.get_model('backupnerstorages', 'Node').find_service_providers_free_space()

        service_providers = SERVICE_PROVIDERS.copy()
        for service_provider in service_providers:
            service_provider.update({
                'free': service_providers_free_space.get(service_provider['code'], 0),
                'logo': static(f'panel/images/datacenters/{service_provider["code"]}.png'),
            })

        response = {
            'data': {
                'project_create': {
                    'validators': validators,
                    'initial': initial,
                },
                'sla_plans': SLA_PLANS,  # todo: refactor to separate API view
                'service_providers': service_providers,  # todo: refactor to separate API view
            },
        }

        return JsonResponse(response)

    @transaction.atomic
    def form_valid(self, form):
        # Create Project
        self.project = form.save(commit=False)
        self.project.account = self.request.user.account
        self.project.save()

        # Allocate Storage resources
        fan_status, node_ids = apps.get_model('backupnerstorages', 'Node').find_available_nodes(dedicated_space=self.project.dedicated_space,
                                                                                                data_center_codes=form.cleaned_data.get('data_centers'))
        if not fan_status:
            raise LookupError  # todo: implement better error handling

        ar_status, storage_ids = apps.get_model('backupnerstorages', 'Storage').allocate_resources(project_id=self.project.id, node_ids=node_ids)
        if not ar_status:
            raise RuntimeError  # todo: implement better error handling

        # Process request
        messages.success(self.request, _('You have added a new project.'))

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.project.get_absolute_url()


class ProjectListView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, AdjustedListView):
    """
    Project list view
    """

    model = Project
    allow_empty = False
    empty_queryset_message = _('You have no projects yet. Please fill the form below to create one.')
    template_name = 'projects/project_list.html'
    ordering = ['name']

    def get_empty_queryset_redirect_url(self):
        return reverse('projects:project_create')

    def get_queryset(self):
        """
        Return the `QuerySet` that will be used to look up the object.
        """
        return super().get_queryset().filter(account=self.request.user.account)


@method_decorator(requires_csrf_token, name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class ProjectDetailView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, ProjectMixin, TemplateView):
    """
    Project detail view
    """

    template_name = 'projects/project_detail.html'


class ProjectSetupView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, ProjectMixin, TemplateView):
    """
    Service Type List View
    """

    template_name = 'projects/project_setup.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'service_types': SERVICE_TYPES,
        })
        return context


class ProjectDataSourceListView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, ProjectMixin, TemplateView):
    """
    Data Source lists by data source types view
    """

    template_name = 'projects/project_data_source_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        mariadb = apps.get_model('backupnerdsmariadb', 'DataSource').active.filter(connection__service__project=self.project).order_by('-name')
        postgresql = apps.get_model('backupnerdspostgresql', 'DataSource').active.filter(connection__service__project=self.project).order_by('-name')
        ssh = apps.get_model('backupnerdsssh', 'DataSource').active.filter(connection__service__project=self.project).order_by('-directory')
        data_sources = sorted(chain(mariadb, postgresql, ssh),
                              key=lambda data_source: data_source.directory.split('/')[-1] if data_source.connection.service.get_service_type() == 'ssh' else data_source.name)
        context.update({
            'object_list': data_sources,
        })

        return context


class ProjectServiceTypeDataSourceListView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, ProjectMixin, AdjustedListView):
    """
    Service Type Data Source List View
    """

    __service_type = None
    allow_empty = True
    template_name = 'projects/project_service_type_data_source_list.html'

    @property
    def service_type(self):
        if self.__service_type is None:
            service_type_code = self.kwargs.get('service_type')
            if service_type_code:
                self.__service_type = next((service_type for service_type in SERVICE_TYPES if service_type['code'] == service_type_code), None)
            else:
                self.__service_type = None
        return self.__service_type

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'service_type': {key_name: self.service_type[key_name] for key_name in self.service_type.keys() if key_name in ['code', 'name']},
        })

        return context

    def get_ordering(self):
        if self.service_type['code'] == 'ssh':
            return ['-directory']
        else:
            return ['-name']

    def get_queryset(self):
        """
        Return the list of items for this view
        """
        return apps.get_model(f"backupnerds{self.service_type['code']}", 'DataSource').active.filter(connection__service__project=self.project).order_by(*self.get_ordering())

    def get(self, request, *args, **kwargs):
        if self.service_type is None:
            raise Http404(_('Page not found.'))

        return super().get(request, *args, **kwargs)


@method_decorator(requires_csrf_token, name='dispatch')
class ProjectSettingsView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, ProjectMixin, TemplateView):
    """
    Project settings view
    """

    template_name = 'projects/project_settings.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'project_ssh_services_count': apps.get_model('backupnerdsssh', 'Service').active.filter(project=self.project).count(),
            'tunnel_list': apps.get_model('backupnerdsssh', 'Connection').active.filter(service__project=self.project).order_by('user'),
            'tunnel_services_description_limit': TUNNEL_SERVICES_DESCRIPTION_LIMIT,
        })

        return context


class ProjectRenameView(LoginRequiredMixin, CanUseServicePermissionRequiredMixin, ProjectMixin, ContextMixin, View):
    """
    Rename Project view
    """

    form_class = RenameProjectForm
    http_method_names = ['patch', 'options']

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _('The project was renamed.'))
        return HttpResponse()

    def form_invalid(self, form):
        return JsonResponse(form.errors.get_json_data(), status=400)

    def patch(self, request, *args, **kwargs):
        form = self.form_class(data=json.loads(request.body), instance=self.project)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ProjectDetail(DeactivateMixin, DestroyAPIView):
    """
    Project Detail: deactivate
    """
    queryset = Project.active.filter(account__is_active=True)
    lookup_field = 'uuid'
    permission_classes = (IsAuthenticated, IsNotSuperuser, IsNotStaff, IsProjectOwnedByUserAccount)
    success_delete_message = _('The project has deleted successfully.')

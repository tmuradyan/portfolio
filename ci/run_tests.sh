#!/bin/bash
set -ex
sleep 7
echo "=> Testing panel image"
python3 -m flake8 /usr/src/app
/usr/src/app/manage.py migrate
/usr/src/app/manage.py check
PYTHONWARNINGS=all,default::ImportWarning::426 \
    coverage run --parallel-mode \
        /usr/src/app/backupner/apps/crm/utils.py -v
PYTHONWARNINGS=all,default::ImportWarning::426 \
    coverage run --parallel-mode \
        /usr/src/app/manage.py test \
            --failfast \
            --verbosity 3 \
            --exclude-tag incompatible \
            --exclude-tag unittests \
            --exclude-tag broken \
            backupner.apps.auth
PYTHONWARNINGS=all,default::ImportWarning::426 \
    coverage run --parallel-mode \
        /usr/src/app/manage.py test \
            --failfast \
            --verbosity 3 \
            backupner.apps.projects
PYTHONWARNINGS=all,default::ImportWarning::426 \
    coverage run --parallel-mode \
        /usr/src/app/manage.py behave --tags ~@skip
coverage combine
coverage report
coverage html

<?php

namespace App\Tools;

use App\Models\Account;

/**
 * Signature class
 * Used to work with signatures
 *
 * @author Tigran Muradyan <t.muradyan@carprice.ru>
 */
class Signature
{
    /**
     * Create HMAC signature
     *
     * @param array $data
     * @return string
     */
    public static function createSignature(array $data)
    {
        $parameters = null;

        ksort($data);

        foreach ($data as $key => $value) {
            if(is_array($value)) {
                $data[$key] = serialize($value);
            }
        }

        $concatenatedData = implode(';', $data);

        return hash_hmac('sha512', $concatenatedData, env('APP_KEY'));
    }

    /**
     * Check signature
     *
     * @param string $scriptName
     * @param array|null $parameters
     * @param string $signature
     * @return bool
     */
    public static function checkSignature(string $scriptName, array $parameters = null, string $signature)
    {
        ksort($parameters);

        foreach ($parameters as $key => $value) {
            if(is_array($value)) {
                $parameters[$key] = serialize($value);
            }
        }

        $concatenatedParameters = implode(';', $parameters);

        if ($concatenatedParameters) {
            $data = $scriptName . ';' . $concatenatedParameters;
        } else {
            $data = $scriptName;
        }

        $ourSignature = hash_hmac('sha512', $data, env('APP_KEY'));

        if ($ourSignature == $signature) {
            return true;
        } else {
            return false;
        }
    }
}

<?php

namespace App\Services;

use App\Models\Account;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\TransactionStatus;
use App\Exceptions\ApiRequestException;
use App\Responses\ResponseErrorDescription;

/**
 * Tranasction service
 *
 * Implemented methods: CreateTransaction
 *                      ProcessTransaction
 *                      HoldTransaction
 * @author Tigran Muradyan <t.muradyan@carprice.ru>
 */
class TransactionService extends AbstractService
{
    /**
     * Service name
     *
     * @var string
     */
    protected $serviceName = 'TransactionService';

    /**
     * Create transaction
     *
     * @param Account $account
     * @param int $typeId
     * @param int $amount
     * @return Transaction
     * @throws \Exception
     */
    public function createTransaction(Account $account, int $typeId, int $amount)
    {
        \DB::beginTransaction();

        try {
            $transaction = Transaction::create([
                'type_id' => $typeId,
                'account_id' => $account->getKey(),
                'amount' => $amount,
                'status_id' => TransactionStatus::where('name', Transaction::STATUS_PENDING)->first()->getKey(),
                'creation_date' => new \DateTime()
            ]);

            \DB::commit();

            return $transaction;
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }

    /**
     * Get transaction
     *
     * @param int $transactionId
     * @return mixed
     * @throws \Exception
     */
    public function getTransaction(int $transactionId)
    {
        try {
            \DB::beginTransaction();

            $transaction = Transaction::find($transactionId);

            \DB::commit();

            if (!$transaction) {
                throw new ApiRequestException('Transaction ' . $transactionId . ' not found', ApiRequestException::TRANSACTION_NOT_FOUND);
            }

            return $transaction;
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }

    /**
     * Success transaction
     *
     * @param Transaction $transaction
     * @return Transaction
     * @throws \Exception
     */
    public function successTransaction(Transaction $transaction)
    {
        \DB::beginTransaction();

        try {
            if (!$transaction) {
                throw new ApiRequestException('Transaction not found', ApiRequestException::TRANSACTION_NOT_FOUND);
            }

            if (!$transaction->isPending() && !$transaction->isHolding()) {
                throw new ApiRequestException(ResponseErrorDescription::TRANSACTION_IS_IN_FINAL_STAGE, ApiRequestException::TRANSACTION_IS_IN_FINAL_STAGE);
            }

            $transaction->successTransaction();

            \DB::commit();

            return $transaction;
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }

    /**
     * Reject transaction
     *
     * @param Transaction $transaction
     * @return Transaction
     * @throws \Exception
     */
    public function rejectTransaction(Transaction $transaction)
    {
        \DB::beginTransaction();

        try {
            if (!$transaction) {
                throw new ApiRequestException('Transaction not found', ApiRequestException::TRANSACTION_NOT_FOUND);
            }

            if (!$transaction->isPending() && !$transaction->isHolding()) {
                throw new ApiRequestException(ResponseErrorDescription::TRANSACTION_IS_IN_FINAL_STAGE, ApiRequestException::TRANSACTION_IS_IN_FINAL_STAGE);
            }

            $transaction->rejectTransaction();

            \DB::commit();

            return $transaction;
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }

    /**
     * Hold transaction
     *
     * @param Transaction|null $transaction
     * @param Account|null $account
     * @param int|null $amount
     * @return Transaction
     * @throws \Exception
     */
    public function holdTransaction(Transaction $transaction = null, Account $account = null, int $amount = null)
    {
        \DB::beginTransaction();

        try {
            if (!$transaction) {
                if ($account && $amount) {
                    $type = TransactionType::where('name', Transaction::TYPE_DEBIT)->first();
                    $transaction = $this->createTransaction($account, $type->getKey(), $amount);

                    if (!$transaction) {
                        throw new ApiRequestException(ResponseErrorDescription::SYSTEM_ERROR, ApiRequestException::SYSTEM_ERROR);
                    }
                } else {
                    throw new ApiRequestException(ResponseErrorDescription::INVALID_PARAMETERS, ApiRequestException::INVALID_PARAMETERS);
                }
            }

            $transaction->holdTransaction();

            \DB::commit();

            return $transaction;
        } catch (\Exception $e) {
            \DB::rollback();

            throw $e;
        }
    }
}

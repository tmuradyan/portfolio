<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\TransactionException;
use App\Responses\ResponseErrorDescription;

/**
 * Transaction entity
 *
 * @author Tigran Muradyan <t.muradyan@carprice.ru>
 */
class Transaction extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_REJECT = 'reject';
    const STATUS_HOLD = 'hold';

    const TYPE_DEBIT = 'debit';
    const TYPE_CREDIT = 'credit';

    protected $table = 'transactions';

    public $timestamps = false;

    protected $fillable = [
        'type_id',
        'account_id',
        'amount',
        'status_id',
        'description',
        'creation_date',
        'update_date',
        'reject_date',
        'hold_date'
    ];

    /**
     * Get Account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->belongsTo('App\Models\Account', 'account_id', 'id')->getResults();
    }

    /**
     * Get type
     *
     * @return TransactionType
     */
    public function getType()
    {
        return $this->belongsTo('App\Models\TransactionType', 'type_id', 'id')->getResults();
    }

    /**
     * Get status
     *
     * @return mixed
     */
    public function getStatus()
    {
        return $this->belongsTo('App\Models\TransactionStatus', 'status_id', 'id')->getResults();
    }

    /**
     * Set status
     *
     * @param $status
     * @return $this
     * @throws TransactionException
     */
    public function setStatus(string $status)
    {
        $transactionStatus = TransactionStatus::where('name', $status)->first();

        if (!$transactionStatus) {
            throw new TransactionException('Undefined status', TransactionException::UNDEFINED_STATUS);
        }

        $this->attributes['status_id'] = $transactionStatus->getKey();

        return $this;
    }

    /**
     * Get creation date
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->attributes['creation_date'] ?? null;
    }

    /**
     * Get success date
     *
     * @return \DateTime
     */
    public function getSuccessDate()
    {
        return $this->attributes['success_date'] ?? null;
    }

    /**
     * Set success date
     *
     * @param \DateTime $date
     * @return $this
     */
    public function setSuccessDate(\DateTime $date)
    {
        $this->attributes['success_date'] = $date;

        return $this;
    }

    /**
     * Get reject date
     *
     * @return \DateTime
     */
    public function getRejectDate()
    {
        return $this->attributes['reject_date'] ?? null;
    }

    /**
     * Set reject date
     *
     * @param \DateTime $date
     * @return $this
     */
    public function setRejectDate(\DateTime $date)
    {
        $this->attributes['reject_date'] = $date;

        return $this;
    }

    /**
     * Get hold date
     *
     * @return \DateTime
     */
    public function getHoldDate()
    {
        return $this->attributes['hold_date'] ?? null;
    }

    /**
     * Set hold date
     *
     * @param \DateTime $date
     * @return $this
     */
    public function setHoldDate(\DateTime $date)
    {
        $this->attributes['hold_date'] = $date;

        return $this;
    }

    /**
     * Get reject description
     *
     * @return mixed
     */
    public function getRejectDescription()
    {
        return $this->attributes['reject_description'] ?? null;
    }

    /**
     * Set reject description
     *
     * @param string $description
     * @return $this
     * @throws TransactionException
     */
    public function setRejectDescription(string $description)
    {
        $this->attributes['reject_description'] = $description;

        return $this;
    }

    /**
     * Success transaction
     *
     * @return bool
     * @throws TransactionException
     */
    public function successTransaction()
    {
        $account = $this->getAccount();

        if ($this->getType()->getName() == self::TYPE_DEBIT) {
            $newBalance = $account->getBalance() + $this->getAmount();
            $newFreeAmount = $account->getFreeAmount() + $this->getAmount();

            $account->setBalance($newBalance);
            $account->setFreeAmount($newFreeAmount);
            $account->save();

            $this->success();
        } elseif ($this->getType()->getName() == Transaction::TYPE_CREDIT) {
            if ($this->getHoldDate()) {
                if ($account->getHoldAmount() < $this->getAmount()) {
                    throw new TransactionException('Not enough hold amount on account. Something is wrong.', TransactionException::NOT_ENOUGH_FREE_AMOUNT);
                }

                $newBalance = $account->getBalance() - $this->getAmount();
                $newHoldAmount = $account->getHoldAmount() - $this->getAmount();

                $account->setBalance($newBalance);
                $account->setHoldAmount($newHoldAmount);
                $account->save();

                $this->success();
            } else {
                if ($account->getFreeAmount() < $this->getAmount()) {
                    $this->reject(ResponseErrorDescription::NOT_ENOUGH_FREE_AMOUNT);
                }

                $newBalance = $account->getBalance() - $this->getAmount();
                $newFreeAmount = $account->getFreeAmount() - $this->getAmount();

                $account->setBalance($newBalance);
                $account->setFreeAmount($newFreeAmount);
                $account->save();

                $this->success();
            }

            return true;
        }
    }

    /**
     * Reject transaction
     *
     * @return bool
     */
    public function rejectTransaction()
    {
        $account = $this->getAccount();

        if ($this->getHoldDate()) {
            $newFreeAmount = $account->getFreeAmount() + $this->getAmount();
            $newHoldAmount = $account->getHoldAmount() - $this->getAmount();

            $account->setFreeAmount($newFreeAmount);
            $account->setHoldAmount($newHoldAmount);

            $account->save();
        }

        $this->reject();

        return true;
    }

    /**
     * Hold transaction
     *
     * @return $this
     */
    public function holdTransaction()
    {
        $account = $this->getAccount();

        if ($account->getFreeAmount() < $this->getAmount()) {
            $this->reject(ResponseErrorDescription::NOT_ENOUGH_FREE_AMOUNT);
        } else {
            $newFreeAmount = $account->getFreeAmount() - $this->getAmount();
            $newHoldAmount = $account->getHoldAmount() + $this->getAmount();

            $account->setFreeAmount($newFreeAmount);
            $account->setHoldAmount($newHoldAmount);
            $account->save();

            $this->hold();
        }

        return $this;
    }

    /**
     * Success transaction
     *
     * @return $this
     */
    public function success()
    {
        $this->setStatus(self::STATUS_SUCCESS);
        $this->setSuccessDate(new \DateTime());

        $this->save();

        return $this;
    }

    /**
     * Reject transaction
     *
     * @return $this
     */
    public function reject(string $description = null)
    {
        $this->setStatus(self::STATUS_REJECT);
        $this->setRejectDate(new \DateTime());

        if ($description) {
            $this->setRejectDescription($description);
        }

        $this->save();

        return $this;
    }

    /**
     * Hold transaction
     *
     * @return $this
     */
    public function hold()
    {
        $this->setStatus(self::STATUS_HOLD);
        $this->setHoldDate(new \DateTime());

        $this->save();

        return $this;
    }

    /**
     * Check if transaction is pending
     *
     * @return bool
     */
    public function isPending()
    {
        if ($this->getStatus()->getAttribute('name') == Transaction::STATUS_PENDING) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if transaction is holding
     *
     * @return bool
     */
    public function isHolding()
    {
        if ($this->getStatus()->getAttribute('name') == Transaction::STATUS_HOLD) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if transaction is succeed
     *
     * @return bool
     */
    public function isSucceed()
    {
        if ($this->getStatus()->getAttribute('name') == Transaction::STATUS_SUCCESS) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if transaction is rejected
     *
     * @return bool
     */
    public function isRejected()
    {
        if ($this->getStatus()->getAttribute('name') == Transaction::STATUS_REJECT) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get amount
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->attributes['amount'] ?? null;
    }
}
